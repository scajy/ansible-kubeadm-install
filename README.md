# ansible-kubeadm在线安装k8s v1.19-v1.20版本



### 1. ansible-kubeadm在线安装k8s v1.19-v1.20版本

- **安装要求**

  - 确保所有节点系统时间一致
  - 操作系统要求：CentOS7.x_x64
  - ansible机器与部署k8s集群做免密钥

- **找一台服务器安装Ansible**

  ```bash
  # yum install epel-release -y
  # yum install ansible -y
  ```

- **下载所需文件**

  克隆部署k8s集群文件

  ```bash
  # git clone https://gitee.com/scajy/ansible-kubeadm-install.git
  # cd ansible-kubeadm-install/
  ```

- **修改Ansible文件**

  - 修改hosts文件，根据规划修改对应IP，变量名称尽量不要修改，按照规则添加对应所需变量名称

    ```yaml
    [master]
    # 如果部署单Master或多master的主master配置
    192.168.0.181    node_name=k8s-master01
    
    [masternode]
    # 用于存储集群部署多节点master，单独存放一个组，对部署编写方便
    192.168.0.182    node_name=k8s-master02 
    
    [node]
    192.168.0.183    node_name=k8s-node01
    192.168.0.184    node_name=k8s-node02
    
    [etcd]
    192.168.0.181 etcd_name=etcd-1
    192.168.0.182 etcd_name=etcd-2
    192.168.0.183 etcd_name=etcd-3
    
    [lb]
    # 如果部署单Master，该项忽略
    192.168.0.185 node_name=lb-master
    192.168.0.186 node_name=lb-backup
    
    [k8s:children]
    master
    masternode
    node
    
    [newnode]
    192.168.0.187   ansible_ssh_port=22  node_name=k8s-node03
    
    
    ```

  - 修改group_vars/all.yml文件，修改etcd证书可信任IP或集群版本

    ```yaml
    #  安装目录
    etcd_work_dir: '/etc/etcd'  
    tmp_dir: '/tmp/k8s'
    
    # k8s执行配置临时目录
    tmp_kubernetes_dir: '/root/kubernetes'
    
    
    # 集群网络
    service_cidr: '10.96.0.0/12'
    pod_cidr: '10.244.0.0/16' # 与roles/addons/files/calico.yaml中网段一致
    
    
    # 集群版本
    k8s_version: 1.20.0  # 版本测试安装了，v1.19.0-v1.20.0版本，按照需要版本修改
    
    
    # 高可用，如果部署单Master，该项忽略
    vip: '192.168.0.188'
    nic: 'eth0'  # 修改为实际内网网卡名
    
    
    # 自签证书可信任IP列表，为方便扩展，可添加多个预留IP
    cert_hosts:
      # 包含所有etcd节点IP
      etcd:
        - 192.168.0.181
        - 192.168.0.182
        - 192.168.0.183
    ```

- 一键部署架构
  
  - 单master架构
    ![输入图片说明](https://foruda.gitee.com/images/1681872717458598031/a23ea2f5_4875258.jpeg "single-master.jpg")
  
  - 多master架构
    ![输入图片说明](https://foruda.gitee.com/images/1681872734743378673/4992cba8_4875258.jpeg "multi-master.jpg")
  
- **执行一键部署k8s集群命令**
  
  - 单master版本
  
    ```bash
    # ansible-playbook -i hosts -uroot single-master-deploy.yml
    ```
  
  - 多master版本
  
    ```bash
    # ansible-playbook -i hosts -uroot multi-master-deploy.yml
    ```
  
- **查看集群节点**
  
  ```shell
  [root@k8s-master01 ~]# kubectl get nodes
  NAME           STATUS   ROLES                  AGE   VERSION
  k8s-master01   Ready    control-plane,master   15h   v1.20.0
  k8s-node01     Ready    <none>                 15h   v1.20.0
  k8s-node02     Ready    <none>                 15h   v1.20.0
  k8s-node03     Ready    <none>                 15h   v1.20.0
  ```
  
- **节点扩容**
  
  - 修改hosts文件，添加新节点IP
  
    ```bash
    # vi hosts
    ...
    [newnode]
    192.168.0.187   ansible_ssh_port=22  node_name=k8s-node03
    ```
  
  - 执行安装添加k8s集群node节点
  
    ```bash
    # ansible-playbook -i hosts -uroot add-node.yml 
    ```
  
- **其他**

  - 部署控制
  
    如果安装某个阶段失败，可针对性测试.
  
    例如：只运行部署插件
  
    ```bash
    # ansible-playbook -i hosts -uroot single-master-deploy.yml  --tags common
    ```
  
- **所有HTTPS证书存放路径**
  
  部署产生的证书都会存放到目录“/tmp/k8s/ssl”，一定要保存好，后面还会用到~
  